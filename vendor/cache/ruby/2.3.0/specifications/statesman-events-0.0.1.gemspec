# -*- encoding: utf-8 -*-
# stub: statesman-events 0.0.1 ruby lib

Gem::Specification.new do |s|
  s.name = "statesman-events"
  s.version = "0.0.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Grey Baker"]
  s.date = "2015-12-05"
  s.description = "Event support for Statesman"
  s.email = ["developers@gocardless.com"]
  s.homepage = "https://github.com/gocardless/statesman-events"
  s.licenses = ["MIT"]
  s.rubygems_version = "2.5.1"
  s.summary = "Event support for Statesman"

  s.installed_by_version = "2.5.1" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<statesman>, [">= 1.3"])
      s.add_development_dependency(%q<rake>, [">= 0"])
      s.add_development_dependency(%q<rspec>, ["~> 3.1"])
      s.add_development_dependency(%q<rspec-its>, ["~> 1.1"])
      s.add_development_dependency(%q<rubocop>, ["~> 0.30.0"])
    else
      s.add_dependency(%q<statesman>, [">= 1.3"])
      s.add_dependency(%q<rake>, [">= 0"])
      s.add_dependency(%q<rspec>, ["~> 3.1"])
      s.add_dependency(%q<rspec-its>, ["~> 1.1"])
      s.add_dependency(%q<rubocop>, ["~> 0.30.0"])
    end
  else
    s.add_dependency(%q<statesman>, [">= 1.3"])
    s.add_dependency(%q<rake>, [">= 0"])
    s.add_dependency(%q<rspec>, ["~> 3.1"])
    s.add_dependency(%q<rspec-its>, ["~> 1.1"])
    s.add_dependency(%q<rubocop>, ["~> 0.30.0"])
  end
end
