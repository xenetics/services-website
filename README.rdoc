A simple MVP of a job board, to put freelancers in contact with potential employers. Focus is on a simple design and ease of use, and an easily understood workflow.
