FactoryGirl.define do
  factory :profile do
    self_introduction "MyText"
    profile_type ""
    user nil

    trait :recruiter_profile do
      profile_type 1
    end

    trait :freelancer_profile do
      profile_type 0
    end
  end
end
