FactoryGirl.define do
  factory :user do
    email 'michael@jordan.com'
    password 'password'
    name 'michael'

    trait :admin do
      profiles { [create(:profile, :freelancer_profile), create(:profile, :recruiter_profile)] }
      role 'admin'
    end

    trait :recruiter do
      profiles { [create(:profile, :recruiter_profile)] }
    end

    trait :freelancer do
      profiles { [create(:profile, :freelancer_profile)] }
    end

    trait :recruiter_and_freelancer do
      profiles { [create(:profile, :freelancer_profile), create(:profile, :recruiter_profile)] }
    end
  end
end
