FactoryGirl.define do
  factory :job_request do
    profile
    name "Ruby on rails developer"
    details "Really interesting job"

    trait :open do
      after(:create) do |job_request|
        create(:job_request_transition, :open, job_request: job_request)
      end
    end

    trait :application_accepted do
      after(:create) do |job_request|
        create(:job_request_transition, :application_accepted, job_request: job_request)
      end
    end

    trait :completed do
      after(:create) do |job_request|
        create(:job_request_transition, :completed, job_request: job_request)
      end
    end
  end

  factory :job_request_transition do
    job_request
    sequence(:sort_key)
    most_recent true

    trait :open do
      to_state "open"
    end

    trait :application_accepted do
      to_state "application_accepted"
    end

    trait :completed do
      to_state "completed"
    end
  end
end
