FactoryGirl.define do
  factory :job_application do
    job_request
    application_message "MyString"
    user_id nil
  end
end
