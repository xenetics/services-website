FactoryGirl.define do
  factory :qualification do
    name "MyString"
    description "MyString"
    profile nil
  end
end
