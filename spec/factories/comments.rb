FactoryGirl.define do
  factory :comment do
    message "MyText"
    user_id 1
    commentable nil
    read false
  end
end
