FactoryGirl.define do
  factory :notification do
    seen false
    user
  end
end
