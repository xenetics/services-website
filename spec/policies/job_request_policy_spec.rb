require 'rails_helper'

RSpec.describe JobRequestPolicy do
  context 'when a recruiter' do
    permissions :index? do
      it 'allows access' do
        recruiter = FactoryGirl.build(:user, :recruiter)

        expect(JobRequestPolicy).to permit(recruiter, JobRequest)
      end
    end

    permissions :show? do
      it 'allows access' do
        recruiter = FactoryGirl.build(:user, :recruiter)
        job_request = FactoryGirl.build(:job_request, profile: recruiter.profiles.first)

        expect(JobRequestPolicy).to permit(recruiter, job_request)
      end
    end

    permissions :edit?, :update?, :destroy? do
      context 'interacts with a job request belonging to him' do
        it 'allows access' do
          recruiter = FactoryGirl.create(:user, :recruiter)
          job_request = FactoryGirl.create(:job_request, profile: recruiter.profiles.first)

          expect(JobRequestPolicy).to permit(recruiter, job_request)
        end
      end

      context 'interacts with a job request belonging to another user' do
        it 'disallows access' do
          recruiter = FactoryGirl.create(:user, :recruiter)
          another_recruiter = FactoryGirl.create(:user, :recruiter, email: 'another@email.com')
          job_request = FactoryGirl.create(:job_request, profile: another_recruiter.profiles.first)

          expect(JobRequestPolicy).not_to permit(recruiter, job_request)
        end
      end
    end

    permissions :new?, :create? do
      it 'allows access' do
        recruiter = FactoryGirl.build(:user, :recruiter)
        job_request = FactoryGirl.build(:job_request, profile: recruiter.profiles.first)

        expect(JobRequestPolicy).to permit(recruiter, JobRequest.new)
      end
    end
  end

  context 'when a non logged in user' do
    permissions :index?, :destroy?, :new?, :create?, :edit?, :update?, :show? do
      it 'disallows access' do
        expect(JobRequestPolicy).not_to permit(nil, JobRequest)
      end
    end
  end
end
