require 'rails_helper'

RSpec.describe JobApplicationPolicy do
  context 'when a recruiter' do

    permissions :index? do
      context 'owns the applications job request' do
        it 'allows access' do
          recruiter = create(:user, :recruiter)
          job_request = create(:job_request, profile: recruiter.profiles.find_by(profile_type: 1))
          job_application = create(:job_application, job_request: job_request)

          expect(JobApplicationPolicy).to permit(recruiter, job_application)
        end
      end
    end

    permissions :show? do
      context 'owns the applications job request' do
        it 'allows access' do
          recruiter = create(:user, :recruiter)
          job_request = create(:job_request, profile: recruiter.profiles.find_by(profile_type: 1))
          job_application = create(:job_application, job_request: job_request)

          expect(JobApplicationPolicy).to permit(recruiter, job_application)
        end
      end
      context 'does not own the applications job request' do
        it 'disallows access' do
          recruiter = create(:user, :recruiter)
          another_recruiter = create(:user, :recruiter, email: "derp@derp.com")
          job_request = create(:job_request, profile: recruiter.profiles.find_by(profile_type: 1))
          job_application = create(:job_application, job_request: job_request)

          expect(JobApplicationPolicy).not_to permit(another_recruiter, job_application)
        end
      end
    end

    permissions :destroy? do
      context 'when the current user is the applicant' do
        it 'allows access' do
          applicant = create(:user, :freelancer)
          job_application = create(:job_application, user_id: applicant.id)

          expect(JobApplicationPolicy).to permit(applicant, job_application)
        end
      end

      context 'when the current user is not the applicant' do
        it 'disallows access' do
          applicant = create(:user, :freelancer)
          another_applicant = create(:user, :freelancer, email: "another@email.com")
          job_application = create(:job_application, user_id: applicant.id)

          expect(JobApplicationPolicy).not_to permit(another_applicant, job_application)
        end
      end
    end
  end
end
