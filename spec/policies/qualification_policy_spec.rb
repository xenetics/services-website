require 'rails_helper'

RSpec.describe QualificationPolicy do

  context 'when a non logged in user' do
    permissions :index? do
      it 'disallows access' do
        expect(QualificationPolicy).not_to permit(nil, Qualification)
      end
    end
  end

  context 'when a user' do
    permissions :edit?, :update?, :destroy? do
      context 'modifies their own qualification' do
        it 'allows access' do
          user = FactoryGirl.create(:user, :freelancer)
          qualification = FactoryGirl.create(:qualification, profile: user.profiles.first)

          expect(QualificationPolicy).to permit(user, qualification)
        end
      end

      context 'modifies another users qualification' do
        it 'disallows access' do
          user = FactoryGirl.create(:user, :freelancer)
          another_user = FactoryGirl.create(:user, :freelancer, email: 'abc@qwerty.com')
          qualification = FactoryGirl.create(:qualification, profile: user.profiles.first)

          expect(QualificationPolicy).not_to permit(another_user, qualification)
        end
      end
    end

    permissions :show? do
      context 'views a qualification' do
        it 'allows access' do
          user = FactoryGirl.create(:user, :freelancer)
          qualification = FactoryGirl.create(:qualification, profile: user.profiles.first)

          expect(QualificationPolicy).to permit(user, qualification)
        end
      end
    end

    permissions :new?, :create? do
      context 'creates a qualification' do
        it 'allows access' do
          user = FactoryGirl.create(:user, :freelancer)

          expect(QualificationPolicy).to permit(user, Qualification.new)
        end
      end
    end

    permissions :index? do
      context 'views a list of qualifications' do
        it 'allows access' do
          user = FactoryGirl.create(:user, :freelancer)

          expect(QualificationPolicy).to permit(user, Qualification)
        end
      end
    end
  end
end
