require 'rails_helper'

RSpec.describe NotificationPolicy do
  context 'when a non user' do
    permissions :index? do
      it 'disallows access' do
        expect(NotificationPolicy).not_to permit(nil, Notification)
      end
    end

    permissions :show? do
      it 'disallows access' do
        user = create(:user)
        notification = create(:notification, user: user)

        expect(NotificationPolicy).not_to permit(nil, notification)
      end
    end
  end

  context 'when a user' do
    context 'access their own notifications' do
      permissions :index? do
        it 'allows access' do
          user = create(:user)
          expect(NotificationPolicy).to permit(user, Notification)
        end
      end

      permissions :show? do
        it 'allows access' do
          user = create(:user)
          notification = create(:notification, user: user)

          expect(NotificationPolicy).to permit(user, notification)
        end
      end
    end
  end

  context 'when a user' do
    context 'access another users own notifications' do
      permissions :show? do
        it 'disallows access' do
          user = create(:user)
          another_user = create(:user, email: "email@new.com")
          notification = create(:notification, user: another_user)

          expect(NotificationPolicy).not_to permit(user, notification)
        end
      end
    end
  end
end
