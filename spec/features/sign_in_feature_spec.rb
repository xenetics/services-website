require 'rails_helper'
require 'session_helper'

feature 'sign in', :devise do
  scenario 'user cannot sign in if not registered' do
    sign_in('person@example.com', 'password')
    expect(body).to have_content 'Invalid email or password.'
  end

  scenario  'user can sign in with valid details' do
    user = create(:user, :recruiter_and_freelancer)
    sign_in(user.email, user.password)
    expect(body).to have_content 'Signed in successfully.'
  end

  scenario 'user cannot sign in with wrong email' do
    user = create(:user, :recruiter_and_freelancer)
    sign_in('invalid@email.com', user.password)
    expect(body).to have_content 'Invalid email or password.'
  end

  scenario 'user cannot sign in with wrong password' do
    user = create(:user, :recruiter_and_freelancer)
    sign_in(user.email, 'invalid_password')
    expect(body).to have_content 'Invalid email or password.'
  end
end
