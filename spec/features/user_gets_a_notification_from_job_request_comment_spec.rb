require 'rails_helper'

feature 'has a notification', type: :feature do
  scenario 'user has a notification about an unread comment' do
    user = create(:user, :recruiter_and_freelancer)
    another_user = create(:user, :recruiter_and_freelancer, email: "test@test.com")
    job_request = create(:job_request, profile: user.profiles.find_by(profile_type: 1))
    comment_text = "I would like to apply!"

    login_as(another_user, scope: :user)
    visit root_path
    click_on "Freelancer"
    click_on "Job Opportunities"
    click_on "Show"

    within('#job-request-comment-form') {
      fill_in("comment_message", with: comment_text)
    }

    expect {
      find('#job-request-comment-form').click_on "Post Comment"
    }.to change{Comment.count}.by(1)

    click_on "Sign out"

    login_as(user)
    expect(user.notifications.count).to eq 1

    visit notifications_path

    expect(page).to have_content("Notification 1")

    click_on "Notification 1"

    expect(page).to have_content(comment_text)
  end
end
