require 'rails_helper'
require 'session_helper'

feature 'views a job request', type: :feature do
  scenario 'user views more details info about a job' do
    user = FactoryGirl.create(:user, :recruiter_and_freelancer)
    job_request = FactoryGirl.create(:job_request, profile: user.profiles.first)

    login_as(user, scope: :user)
    visit job_requests_path

    expect(page).to have_content(job_request.name)

    click_on "Show"

    expect(page).to have_content(job_request.name)
  end
end
