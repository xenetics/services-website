require 'rails_helper'

feature "job application", type: :feature do
  scenario "a recruiter gets a job application" do
    recruiter = create(:user, :recruiter_and_freelancer)
    applicant = create(:user, :recruiter_and_freelancer, email: "applicant@email.com")
    job_request = create(:job_request, profile: recruiter.profiles.find_by(profile_type: 1))
    job_request_application = create(:job_application, job_request: job_request, user_id: applicant.id)

    login_as(recruiter, scope: :user)
    visit root_path
    click_on "Recruiter"
    click_on "Applications"

    expect(page).to have_content(applicant.name)
    expect(page).to have_content(job_request_application.application_message)
  end
end
