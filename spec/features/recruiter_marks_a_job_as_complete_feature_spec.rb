require 'rails_helper'

feature 'mark a job as complete', type: :feature do
  scenario 'recruiter marks a job as complete' do
    recruiter = create(:user, :recruiter_and_freelancer)
    job_request = create(:job_request, :application_accepted, profile: recruiter.profiles.find_by(profile_type: 1))

    login_as(recruiter, scope: :user)
    visit root_path
    click_on "Recruiter"
    click_on "Show"
    click_on "Mark as complete"

    expect(page).to have_content "Job completed!"
    expect(job_request.state_machine.current_state).to eq("completed")
  end
end
