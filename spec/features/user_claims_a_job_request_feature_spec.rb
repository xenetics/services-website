require 'rails_helper'
require 'session_helper'

feature 'claiming a job request', :devise do
  scenario  'freelancer claims a job request from dashboard' do
    user = FactoryGirl.create(:user, :recruiter_and_freelancer)
    job_request = FactoryGirl.create(:job_request, profile: user.profiles.first)
    user_freelancer_profile = user.profiles.find_by(profile_type: 0)

    sign_in(user.email, user.password)

    expect(body).to have_content 'Signed in successfully.'
    click_on 'Freelancer'
    visit job_requests_path
    click_on "Show"
    expect(page).to have_content(job_request.name)

    expect do
      click_on 'Apply'
    end.to change(user_freelancer_profile.claimed_jobs, :count).by(1)

    # can only apply once for a job
    expect do
      click_on 'Apply'
    end.to change(user_freelancer_profile.claimed_jobs, :count).by(0)

    visit user_path(user)

    expect(page).to have_content(job_request.name)
  end
end
