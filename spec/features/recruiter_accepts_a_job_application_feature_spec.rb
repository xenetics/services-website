require 'rails_helper'

feature 'accepting a job application', type: :feature do
  scenario 'recruiter accepts a job application and is redirected' do
    recruiter = create(:user, :recruiter_and_freelancer)
    applicant = create(:user, :recruiter_and_freelancer, email: "applicant@email.com")
    job_request = create(:job_request, profile: recruiter.profiles.find_by(profile_type: 1))

    login_as(applicant, scope: :user)
    visit root_path
    click_on "Freelancer"
    visit job_requests_path
    click_on "Show"
    click_on "Apply"
    click_on "Sign out"

    login_as(recruiter, scope: :user)
    visit root_path
    click_on "Recruiter"
    click_on "Applications"

    expect { click_on "Accept Application" }.to change { job_request.current_state }.
      from("open").to("application_accepted")
    expect(page).to have_content "Application Accepted!"

    click_on "Sign out"
    login_as(applicant, scope: :user)
    visit root_path
    click_on "Freelancer"

    expect(page).to have_content (job_request.name)
  end
end
