require 'rails_helper'
require 'session_helper'

feature 'update details', type: :feature do
  scenario  'user signs in and update details' do
    user = create(:user, :recruiter_and_freelancer)
    login_as(user, scope: :user)
    visit root_path
    click_on "Edit Details"
    fill_in :current_password, with: user.password
    click_on 'Update'

    expect(page).to have_content('Your account has been updated successfully.')
  end
end
