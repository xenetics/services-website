require 'rails_helper'

feature 'send a message', type: :feature do
  scenario 'freelancer messages a recruiter' do
    freelancer = create(:user, :recruiter_and_freelancer)
    recruiter = create(:user, :recruiter_and_freelancer, email: "test@test.com")
    message_text = "What kind of job is it?"
    job_request = create(:job_request, profile: recruiter.profiles.find_by(profile_type: 0))

    login_as(freelancer, scope: :user)
    visit root_path

    click_on "Freelancer"
    click_on "Job Opportunities"
    click_on "Show"

    within('#message-form') {
      fill_in("comment_message", with: message_text)
    }

    expect {
      find('#message-form').click_on "Send Message"
    }.to change{ Comment.count }.by(1)

    click_on "Sign out"
    login_as(recruiter, scope: :user)

    expect(recruiter.notifications.count).to eq 1

    visit notifications_path

    expect(page).to have_content("Notification 1")

    click_on "Notification 1"

    expect(page).to have_content(message_text)
  end
end
