require 'rails_helper'
require 'session_helper'

feature 'User index page', :devise do
  scenario 'user can view own email address' do
    user = FactoryGirl.create(:user, :admin)
    login_as(user, scope: :user)
    visit users_path
    expect(page).to have_content user.name
  end
end