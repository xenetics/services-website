require 'rails_helper'

RSpec.describe JobRequest, type: :model do
  describe '#owned_by_current_user' do
    it 'returns a list of job requests belonging to the current user' do
      user = create(:user, :recruiter)
      owned_job_request = create(:job_request, profile: user.profiles.find_by(profile_type: 1))
      non_owned_job_request = create(:job_request)
      job_requests = [owned_job_request, non_owned_job_request]

      expect(JobRequest.owned_by_current_user(user.profiles.find_by(profile_type: 1))).to match_array(owned_job_request)
    end
  end

  describe '#not_owner' do
    it 'returns a list of job requests excluding those belonging to the current user' do
      user = create(:user, :recruiter)
      owned_job_request = create(:job_request, profile: user.profiles.find_by(profile_type: 1))
      non_owned_job_request = create(:job_request)
      job_requests = [owned_job_request, non_owned_job_request]

      expect(JobRequest.not_owner(user.profiles.find_by(profile_type: 1))).to match_array(non_owned_job_request)
    end
  end

  describe '#accepted' do
    it 'returns a list of job requests that have had an applicaiton accepted' do
      applied_and_not_accepted_job_request = create(:job_request, :open)
      applied_and_accepted_job_request = create(:job_request, :application_accepted)

      expect(JobRequest.application_accepted).to match_array(applied_and_accepted_job_request)
    end
  end
end
