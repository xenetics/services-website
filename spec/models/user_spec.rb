require 'rails_helper'

RSpec.describe User, type: :model do

  describe '#notifications' do
    it "returns a list of notifications that are not seen" do
      user = create(:user)
      seen_notification =  create(:notification, seen: true, user: user)
      unseen_notification = create(:notification, seen: false, user: user)

      notifications = [unseen_notification, seen_notification]
      unseen_notifications = user.notifications.unseen_notifications

      expect(unseen_notifications).to match_array([unseen_notification])
    end
  end
end
