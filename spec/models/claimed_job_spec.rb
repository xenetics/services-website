require 'rails_helper'

RSpec.describe ClaimedJob, type: :model do
  describe '#requests' do
    it 'returns a list of job requests the current user has applied form' do
      applicant = create(:user, :recruiter_and_freelancer)
      applied_job_request = create(:job_request)
      another_job_request = create(:job_request)
      applicant_claimed_jobs = create(:claimed_job, job_request_id: applied_job_request.id, profile: applicant.profiles.find_by(profile_type: 0))
      another_claimed_job = create(:claimed_job, job_request_id: another_job_request.id)

      expected_job_requests = applicant.profiles.find_by(profile_type: 0).claimed_jobs

      expect(ClaimedJob.requests(expected_job_requests)).to match_array(applied_job_request)
    end
  end
end
