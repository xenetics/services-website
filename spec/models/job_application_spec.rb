require 'rails_helper'

RSpec.describe JobApplication, type: :model do

  describe '#filter_by_job' do
    it 'returns a list of applications for a job request' do
      job_request = create(:job_request)
      another_job_request = create(:job_request)
      expected_job_request_application = create(:job_application, job_request: job_request)
      another_job_request_application = create(:job_application, job_request: another_job_request)

      applications = JobApplication.all.filter_by_job(job_request)

      expect(applications).to match_array(expected_job_request_application)
    end
  end

  describe '#filter_by_jobs' do
    it 'returns a list of applications for a list of job requests' do
      job_request = create(:job_request)
      another_job_request = create(:job_request)
      a_third_job_request = create(:job_request)
      expected_job_request_applications = [create(:job_application, job_request: job_request), create(:job_application, job_request: another_job_request)]
      another_job_request_application = create(:job_application, job_request: a_third_job_request)

      applications = JobApplication.all.filter_by_jobs([job_request, another_job_request])

      expect(applications).to match_array(expected_job_request_applications)
    end
  end

  describe '#filter_by_unseen' do
    it 'returns a list of unseen job applications' do
      expected_job_request_application = create(:job_application, seen: false)
      another_job_request_application = create(:job_application, seen: true)

      applications = JobApplication.all.filter_by_unseen

      expect(applications).to match_array(expected_job_request_application)
    end
  end
end
