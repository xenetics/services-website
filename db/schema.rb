# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160720043715) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "applications", force: :cascade do |t|
    t.integer  "job_request_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "user_id"
    t.text     "message"
  end

  add_index "applications", ["job_request_id"], name: "index_applications_on_job_request_id", using: :btree

  create_table "children", force: :cascade do |t|
    t.string   "name"
    t.integer  "parent_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "children", ["parent_id"], name: "index_children_on_parent_id", using: :btree

  create_table "claimed_jobs", force: :cascade do |t|
    t.integer  "job_request_id"
    t.integer  "profile_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "claimed_jobs", ["profile_id"], name: "index_claimed_jobs_on_profile_id", using: :btree

  create_table "comments", force: :cascade do |t|
    t.text     "message"
    t.integer  "user_id"
    t.integer  "commentable_id"
    t.string   "commentable_type"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.boolean  "read",             default: false, null: false
  end

  add_index "comments", ["commentable_type", "commentable_id"], name: "index_comments_on_commentable_type_and_commentable_id", using: :btree

  create_table "job_applications", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "job_request_id"
    t.string   "application_message"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.boolean  "seen",                default: false
  end

  add_index "job_applications", ["job_request_id"], name: "index_job_applications_on_job_request_id", using: :btree

  create_table "job_request_transitions", force: :cascade do |t|
    t.string   "to_state",                      null: false
    t.text     "metadata",       default: "{}"
    t.integer  "sort_key",                      null: false
    t.integer  "job_request_id",                null: false
    t.boolean  "most_recent",                   null: false
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "job_request_transitions", ["job_request_id", "most_recent"], name: "index_job_request_transitions_parent_most_recent", unique: true, where: "most_recent", using: :btree
  add_index "job_request_transitions", ["job_request_id", "sort_key"], name: "index_job_request_transitions_parent_sort", unique: true, using: :btree

  create_table "job_requests", force: :cascade do |t|
    t.string   "name"
    t.text     "details"
    t.integer  "profile_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "freelancer_id"
    t.text     "further_information"
  end

  add_index "job_requests", ["profile_id"], name: "index_job_requests_on_profile_id", using: :btree

  create_table "notifications", force: :cascade do |t|
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "user_id",                      null: false
    t.integer  "from_user_id"
    t.boolean  "seen",         default: false
    t.integer  "comment_id"
  end

  create_table "parents", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "posts", force: :cascade do |t|
    t.text     "content"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "category"
  end

  add_index "posts", ["user_id"], name: "index_posts_on_user_id", using: :btree

  create_table "profiles", force: :cascade do |t|
    t.text     "self_introduction"
    t.integer  "profile_type"
    t.integer  "user_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "profiles", ["user_id"], name: "index_profiles_on_user_id", using: :btree

  create_table "qualifications", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "profile_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "qualifications", ["profile_id"], name: "index_qualifications_on_profile_id", using: :btree

  create_table "requests", force: :cascade do |t|
    t.text     "content"
    t.string   "service"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "requests", ["user_id", "created_at"], name: "index_requests_on_user_id_and_created_at", using: :btree
  add_index "requests", ["user_id"], name: "index_requests_on_user_id", using: :btree

  create_table "skills", force: :cascade do |t|
    t.string   "description"
    t.integer  "profile_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "skills", ["profile_id"], name: "index_skills_on_profile_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.integer  "role"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "applications", "job_requests"
  add_foreign_key "children", "parents"
  add_foreign_key "claimed_jobs", "profiles"
  add_foreign_key "job_applications", "job_requests"
  add_foreign_key "job_requests", "profiles"
  add_foreign_key "posts", "users"
  add_foreign_key "profiles", "users"
  add_foreign_key "qualifications", "profiles"
  add_foreign_key "requests", "users"
  add_foreign_key "skills", "profiles"
end
