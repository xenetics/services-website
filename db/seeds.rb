# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
puts 'Seeding users...'
user1 = User.create!(name: "Lachlan", email: "lachlan.miller@tworedkites.com",
                    password: "123456789", password_confirmation: "123456789")
user2 = User.create!(name: "Mitchell", email: "mitchell.buckley@tworedkites.com",
                    password: "123456789", password_confirmation: "123456789")
user3 = User.create!(name: "Joey", email: "joey@friends.com",
                    password: "123456789", password_confirmation: "123456789")

puts 'Create freelancer profile...'
user1.profiles.create!(self_introduction: "I pride myself on punctuality and high quality service. I am happy to give you a free, no obligation quote.",
                        profile_type: 0)
user2.profiles.create!(self_introduction: "I work hard and have a large history of success with clients.",
                        profile_type: 0)
user2.profiles.create!(self_introduction: "I have been in the industry for many years.",
                        profile_type: 0)

puts 'Creating recruiter profile...'
user1.profiles.create!(self_introduction: "I run a web dev business and need contract programmers for short term work.",
                        profile_type: 1)

user2.profiles.create!(self_introduction: "I run a building company and need manual labourers",
                        profile_type: 1)

user3.profiles.create!(self_introduction: "I play an actor on a 90s sitcom.",
                        profile_type: 1)

puts 'Creating requests...'
user2.profiles.find_by(profile_type: 1).job_requests.create!(name: 'Three week UI/UX specialist', details: "Someone is needed to design and implement a new UI for a website using semantic UI.", further_information: Faker::Lorem.paragraphs.join("\n\n"))

user2.profiles.find_by(profile_type: 1).job_requests.create!(name: 'Back end developer', details: "Looking for someone with ruby and javascript experience to build a stocktake system.", further_information: Faker::Lorem.paragraphs.join("\n\n"))

user2.profiles.find_by(profile_type: 1).job_requests.create!(name: 'Cleaning position', details: "Due to an overwhelming demand from our clients, we are currently recruiting domestic cleaners to join our team in assisting professional couples, individuals and nice families with their home cleaning and general housework. Tasks involved are the same as what you do in your own home every day – the only difference is that you get paid for it!", further_information: Faker::Lorem.paragraphs.join("\n\n"))
user2.profiles.find_by(profile_type: 1).job_requests.create!(name: 'Head of cleaning', details: "As the leading public health service in the Eastern Region, we are looking for a Housekeeper on Permanent Part Time basis.", further_information: "This job is for scheduled cleaning on Saturdays, once a week from 9am-12pm. Potential for more work exists if the client is happy with the services you are providing. Call 123-456-789 for more details or message via this website.", further_information: Faker::Lorem.paragraphs.join("\n\n"))
user3.profiles.find_by(profile_type: 1).job_requests.create!(name: 'Electrician', details: "Looking for experienced electrician for long term contract.", further_information: Faker::Lorem.paragraphs.join("\n\n"))
user3.profiles.find_by(profile_type: 1).job_requests.create!(name: 'Eletrical Engineer', details: "We are seeking an experienced and licensed Electrician to operate throughout the Brisbane region. It is estimated there will be between 10-20 hours per week initially with plenty of work to increase these hours if desired.", further_information: Faker::Lorem.paragraphs.join("\n\n"))

puts 'Creating applications...'

JobRequest.all.each do |request|
  (1 + Random.rand(6)).times do
    #request.job_applications.create!(application_message: Faker::Lorem.sentence(8), user_id: 1 + Random.rand(User.all.count))
  end
end

puts 'Updating qualifications and skills...'
user1.profiles.find_by(profile_type: 0).qualifications.create!(name: "Certificate 3 in Electrical Safety",
                        description: "Able to safely perform electrical work in accordance to QLD OHS laws.")

user1.profiles.find_by(profile_type: 0).qualifications.create!(name: "Diploma in Building",
                        description: "Able to perform basic foundations work and brick laying.")

user1.profiles.find_by(profile_type: 0).skills.create!(description: "Polite, honest and about to manage time effectively.")
