class AddFreelancerIdToJobRequests < ActiveRecord::Migration
  def change
    add_column :job_requests, :freelancer_id, :integer
  end
end
