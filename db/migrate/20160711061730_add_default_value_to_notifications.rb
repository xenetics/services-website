class AddDefaultValueToNotifications < ActiveRecord::Migration
  def change
    change_column :notifications, :user_id, :integer, null: false
  end
end
