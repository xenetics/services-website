class CreateClaimedJobs < ActiveRecord::Migration
  def change
    create_table :claimed_jobs do |t|
      t.integer :job_request_id
      t.references :profile, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
