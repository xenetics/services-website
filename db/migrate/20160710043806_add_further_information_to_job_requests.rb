class AddFurtherInformationToJobRequests < ActiveRecord::Migration
  def change
    add_column :job_requests, :further_information, :text
  end
end
