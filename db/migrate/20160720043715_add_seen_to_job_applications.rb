class AddSeenToJobApplications < ActiveRecord::Migration
  def change
    add_column :job_applications, :seen, :boolean, default: false
  end
end
