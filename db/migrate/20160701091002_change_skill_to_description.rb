class ChangeSkillToDescription < ActiveRecord::Migration
  def self.up
    rename_column :skills, :skill, :description
  end

  def self.down
    rename_column :skills, :skill, :skill
  end
end
