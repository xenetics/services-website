class ChangeDescriptionTypeInQualifications < ActiveRecord::Migration
  def self.up
    change_column :qualifications, :description, :text
  end

  def self.down
    change_column :qualifications, :description, :string
  end
end
