class AddFieldsToApplication < ActiveRecord::Migration
  def change
    add_column :applications, :applicant_id, :integer
    add_column :applications, :message, :text
  end
end
