class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.text :content
      t.string :service
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :requests, [:user_id, :created_at]
  end
end
