class AddReadValToComments < ActiveRecord::Migration
  def change
    add_column :comments, :read, :boolean, default: false, null: false
  end
end
