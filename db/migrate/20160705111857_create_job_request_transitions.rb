class CreateJobRequestTransitions < ActiveRecord::Migration
  def change
    create_table :job_request_transitions do |t|
      t.string :to_state, null: false
      t.text :metadata, default: "{}"
      t.integer :sort_key, null: false
      t.integer :job_request_id, null: false
      t.boolean :most_recent, null: false
      t.timestamps null: false
    end

    add_index(:job_request_transitions,
              [:job_request_id, :sort_key],
              unique: true,
              name: "index_job_request_transitions_parent_sort")
    add_index(:job_request_transitions,
              [:job_request_id, :most_recent],
              unique: true,
              where: 'most_recent',
              name: "index_job_request_transitions_parent_most_recent")
  end
end
