class CreateJobRequests < ActiveRecord::Migration
  def change
    create_table :job_requests do |t|
      t.string :name
      t.text :details
      t.references :profile, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
