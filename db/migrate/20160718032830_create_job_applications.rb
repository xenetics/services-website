class CreateJobApplications < ActiveRecord::Migration
  def change
    create_table :job_applications do |t|
      t.integer :user_id
      t.references :job_request, index: true, foreign_key: true
      t.string :application_message

      t.timestamps null: false
    end
  end
end
