class ContextFinder
  def initialize(params, whitelist)
    @params = params
    @whitelist = whitelist
  end

  def find
    if exists_in_whitelist?
      klass.camelize.constantize.find params[:"#{klass}_id"]
    else
      raise ContextNotFound.new(params), 'Cannot find contextual object. Has it been added to the whitelist?'
    end
  end

  def self.find(params, whitelist)
    new(params, whitelist).find
  end

  private

  attr_reader :params, :klass, :whitelist

  def exists_in_whitelist?
    @klass = whitelist.detect { |pk| params[:"#{pk}_id"].present? }
  end
end

class ContextNotFound < StandardError
  attr_reader :params

  def initialize(params)
    @params = params
  end
end
