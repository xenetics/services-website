class JobRequestStateMachine
  include Statesman::Machine
  include Statesman::Events

  state :open, initial: true
  state :application_accepted
  state :completed

  event :job_accepted do
    transition from: :open, to: :application_accepted
  end

  event :job_completed do
    transition from: :application_accepted, to: :completed
  end
end
