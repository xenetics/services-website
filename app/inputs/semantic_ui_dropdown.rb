class SemanticUiDropdownInput < Formtastic::Inputs::SelectInput
  def select_html
    builder.select(input_name, collection, input_options,
                   input_html_options.merge(class: 'ui fluid search selection dropdown'))
  end

  def include_blank
    options[:include_blank]
  end

  def wrapper_classes
    super + ' field'
  end
end
