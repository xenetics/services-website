class JobRequest < ActiveRecord::Base

  # Allow searching via states
  include Statesman::Adapters::ActiveRecordQueries
  def self.transition_name
    :transitions
  end

  def self.transition_class
    JobRequestTransition
  end
  private_class_method :transition_class

  def self.initial_state
    JobRequestStateMachine.initial_state
  end
  private_class_method :initial_state

  belongs_to :profile
  has_many :transitions, class_name: "JobRequestTransition", autosave: false
  has_many :comments, as: :commentable, dependent: :destroy
  has_many :job_applications, dependent: :destroy

  scope :order_by_date, -> { order(created_at: :desc) }
  scope :open, -> { in_state(:open) }

  scope :not_owner, -> (profile) { where('profile_id != ?', profile.id) }
  scope :owned_by_current_user, -> (profile) { where('profile_id = ?', profile.id) }
  scope :application_accepted, -> { in_state(:application_accepted) }

  delegate :current_state, :in_state?, to: :state_machine

  def state_machine
    @state_machine ||= JobRequestStateMachine.new(self, transition_class: JobRequestTransition,
                                                   association_name: :transitions)
  end
end
