class User < ActiveRecord::Base
  enum role: [:user, :editor, :admin]
  after_initialize :set_default_role, if: :new_record?
  after_create :set_up_profiles


  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :rememberable, :trackable, :validatable

  has_many(:profiles)
  has_many :notifications
  has_many :comments, as: :commentable
  private

  def set_default_role
    self.role ||= :user
  end

  def set_up_profiles
    self.profiles.create!(self_introduction: "Update your self introduction!", profile_type: 0)
  end
end
