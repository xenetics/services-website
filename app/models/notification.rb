class Notification < ActiveRecord::Base
  belongs_to :user
  has_many :comments, as: :commentable, dependent: :destroy

  scope :unseen_notifications, -> { where(seen: false) }
end
