class Profile < ActiveRecord::Base
  enum profile_type: [:employee, :employer]
  after_initialize :set_default_role, :if => :new_record?

  belongs_to :user
  has_many :qualifications
  has_many :skills
  has_many :job_requests
  has_many :claimed_jobs
  has_many :comments, as: :commentable

  def set_default_role
    self.profile_type ||= :employee
  end
end
