class ClaimedJob < ActiveRecord::Base
  belongs_to :profile

  scope :requests, -> (claimed_jobs) { JobRequest.where(id: claimed_jobs.pluck(:job_request_id)) }

end
