class JobApplication < ActiveRecord::Base
  belongs_to :job_request

  scope :filter_by_job, -> (job_request) { where("job_request_id = ?", job_request.id) }
  scope :filter_by_jobs, -> (job_requests) { where(job_request_id: job_requests.collect(&:id)) }
  scope :filter_by_unseen, -> { where(seen: false) }
end
