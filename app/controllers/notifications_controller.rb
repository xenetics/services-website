class NotificationsController < ApplicationController
  include ApplicationHelper

  def show
    @notification = Notification.find(params[:id])
    @notification.update_attributes(seen: true)
    @comment = Comment.find(@notification.comment_id)
  end

  def index
    authorize(Notification)
    @notifications = current_user.notifications.all.unseen_notifications
    @job_application_notifications = job_application_notifications
    @job_requests = []
    @job_application_notifications.each do |n|
      @job_requests.push(JobRequest.find(n.job_request_id))
    end
  end
end
