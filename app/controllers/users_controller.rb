class UsersController < ApplicationController
  before_filter :authenticate_user!

  def index
    @users = User.all
    authorize(User) # activate policy
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      redirect_to root_path
      # save
    else
      render 'edit', alert: "Error"
    end
  end

  def show
    if user_signed_in?
      @user = User.find(params[:id])

      if current_user_profile.eql? '0'
        @profile = @user.profiles.find_by(profile_type: 0)
        @qualifications = @profile.qualifications.all
        @skills = @profile.skills.all
        @active_jobs = @profile.claimed_jobs.requests(@profile.claimed_jobs)
        @accepted_applications = @active_jobs.application_accepted
        @active_jobs = @active_jobs - @accepted_applications
      else
        @profile = @user.profiles.find_by(profile_type: 1)
        @job_requests = @profile.job_requests.all.order_by_date
        @open_jobs = @job_requests.open
        @accepted_applications = @job_requests.application_accepted
      end
    else
      # display a flash
      flash[:danger] = "Please log in to view user profiles."
      redirect_to root_path
    end
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end
end
