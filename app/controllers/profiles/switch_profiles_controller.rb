class Profiles::SwitchProfilesController < ApplicationController
  def update
    session[:current_profile_id] = params[:profile_id]
    redirect_to current_user
  end
end
