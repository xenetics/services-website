class CommentsController < ApplicationController
  PARENTS = %w(user job_request).freeze

  def create
    @context = ContextFinder.find(params, PARENTS)
    @comment = @context.comments.new(comments_params)

    if @comment.save
      redirect_to :back, notice: "Commented posted"
      if @context.class.name == "JobRequest"
        @owner = User.find(Profile.find(@context.profile_id).user_id)
      elsif @context.class.name == "User"
        @owner = @context
      end
      @owner.notifications.create(comment_id: @comment.id)
    else
      redirect_to @context, notice: "Comment not saved"
    end
  end

  private

  def comments_params
    params.require(:comment).permit(:message).merge(user: current_user)
  end
end
