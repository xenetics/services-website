class ProfilesController < ApplicationController
  def update
    @profile = Profile.find(params[:id])

    if @profile.update(profile_params)
      redirect_to @profile.user
      flash[:success] = "Updated self introduction."
    else
      redirect_to @profile.user
      flash[:danger] = "Error updating self introduction. Please check the fields are correct and try again."
    end
  end

  def show
    @profile = Profile.find(params[:id])
  end

  private

  def profile_params
    params.require(:profile).permit(:self_introduction)
  end
end
