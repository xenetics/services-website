class ApplicationController < ActionController::Base

  include Pundit
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  protect_from_forgery with: :exception


  def current_user_profile
    if user_signed_in?
      @current_user_profile = session[:current_profile_id]
    end
  end

  def current_recruiter
    current_user.profiles.find_by(profile_type: 1)
  end

  def current_freelancer
    current_user.profiles.find_by(profile_type: 0)
  end

  private

  def user_not_authorized
    flash[:error] = "Access denied"
    redirect_to(request.referrer || root_path)
  end
end
