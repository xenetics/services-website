class JobApplicationsController < ApplicationController
  before_action :set_job_request_and_applications, only: [:show, :index, :update]
  after_filter :update_seen, only: :index

  def show
  end

  def update
    if @job_request.state_machine.current_state == 'open'
      @job_request.state_machine.transition_to!(:application_accepted)
      flash[:success] = "Application Accepted!"
      redirect_to current_user
    else
      flash[:danger] = "An application has already been accepted for this job"
      redirect_to current_user
    end
  end

  def index
    if !@job_applications.first.nil?
      authorize(@job_applications.first)
      @applicants = []
      @job_applications.each do |app|
        @applicants.push(User.find(app.user_id))
      end
    end
  end

  private

  def set_job_request_and_applications
    @job_request = JobRequest.find(params[:job_request_id])
    @job_applications = @job_request.job_applications.filter_by_job(@job_request)
  end

  def update_seen
    @job_applications.each do |app|
      if job_request_is_owned_by_current_user?(@job_request)
        app.update_attributes(seen: true)
      end
    end
  end

  def job_request_is_owned_by_current_user?(job_request)
    job_request.profile_id == current_user.profiles.find_by(profile_type: 1).id
  end
end
