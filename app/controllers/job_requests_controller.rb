class JobRequestsController < ApplicationController
  before_action :set_job_request, only: [:show, :edit, :destroy, :update]

  def create
    @job_request = current_recruiter.job_requests.new(job_request_params)
    authorize(@job_request)

    if @job_request.save
      redirect_to session[:referer], notice: "Saved new job request."
    else
      redirect_to session[:referer], alert: "Error. Please check all the fields are correct and try again."
    end
  end

  def edit
    save_referer
  end

  def update
    if @job_request.update(job_request_params)
      redirect_to session[:referer], notice: "Updated job request."
    else
      redirect_to session[:referer], notice: "Unable to update job request. Check fields are correct and try again."
    end
  end

  def destroy
    @job_request.destroy
    flash[:success] = 'Job request was successfully deleted.'
    redirect_to :back
  end

  def index
    authorize(JobRequest)
    @job_requests = JobRequest.all.not_owner(current_recruiter).order_by_date.open
  end

  def show
    @current_profile_type = current_user_profile
    @comment = @job_request.comments.new
    @comments = @job_request.comments.all

    @recruiter_profile = Profile.find(@job_request.profile_id)
    @recruiter = User.find(@recruiter_profile.user_id)

    @message = @recruiter.comments.new
  end

  def new
    @job_request = current_recruiter.job_requests.new
    authorize(@job_request)
    save_referer
  end

  private

  def save_referer
    session[:referer] = request.referer
  end

  def set_job_request
    @job_request = JobRequest.find(params[:id])
    authorize(@job_request)
  end

  def job_request_params
    params.require(:job_request).permit(:name, :details)
  end
end
