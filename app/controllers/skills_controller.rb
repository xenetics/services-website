class SkillsController < ApplicationController
  before_action :set_user_profile
  def index
    @skills = @profile.skills.all

    @skill = @profile.skills.build # new skill form
  end

  def update
    @skill = Skill.find_by(id: params[:id])

    if @skill.update(skills_params)
      redirect_to skills_path, notice: "Updated skill."
    else
      redirect_to skills_path, notice: "Unable to update skill. Check fields are correct and try again."
    end
  end

  def edit
    @skill = Skill.find_by(id: params[:id])
  end

  def create
    @skill = @profile.skills.new(skills_params)

    if @skill.save
      redirect_to skills_path, notice: "Saved new skill."
    else
      redirect_to :back, alert: "Error. Please check all the fields are correct and try again."
    end
  end

  def destroy
    puts "Params", params
    puts "params id " , params[:id]
    @skill = Skill.find_by(id: params[:id])
    puts @skill.inspect

    @skill.destroy
    flash[:success] = 'skill was successfully deleted.'
    redirect_to action: 'index'
  end

  private

  def set_user_profile
    @profile = current_user.profiles.find_by(profile_type: 0)
  end

  def skills_params
    params.require(:skill).permit(:description)
  end
end
