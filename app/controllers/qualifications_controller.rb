class QualificationsController < ApplicationController
  before_action :set_qualification, only: [:edit, :update, :destroy]
  before_action :set_user_profile

  def edit
  end

  def update
    if @qualification.update(qualification_params)
      redirect_to qualifications_path, notice: "Updated qualification."
    else
      redirect_to qualifications_path, notice: "Unable to update qualification. Check fields are correct and try again."
    end
  end

  def create
    @qualification = @profile.qualifications.new(qualification_params)

    if @qualification.save
      redirect_to qualifications_path, notice: "Saved new qualification."
    else
      redirect_to :back, alert: "Error. Please check all the fields are correct and try again."
    end
  end

  def destroy
    @qualification.destroy
    flash[:success] = 'Qualification was successfully deleted.'
    redirect_to action: 'index'
  end

  def index
    authorize(Qualification)
    @qualifications = @profile.qualifications.all

    @qualification = @profile.qualifications.build
  end

  private

  def set_qualification
    @qualification = Qualification.find(params[:id])
    authorize(@qualification)
  end

  def set_user_profile
    @profile = current_user.profiles.find_by(profile_type: 0) unless current_user.nil?
  end

  def qualification_params
    params.require(:qualification).permit(:name, :description)
  end
end
