class ClaimedJobsController < ApplicationController
  def create
    job_request = JobRequest.find(params[:request])
    job_request.job_applications.create!(user_id: current_user.id)

    # create claimed job for user, mirror job request
    current_user.profiles.find_by(profile_type: 0).claimed_jobs.create!(job_request_id: job_request.id)
    redirect_to :back
  end
end
