class JobRequests::TransitionsController < ApplicationController

  def update
    @job_request = JobRequest.find(params[:job_request_id])

    if @job_request.state_machine.current_state == "open"
      was_created = false
      application = @job_request.job_applications.find_or_create_by(user_id: current_user.id) do |application|
        if application.new_record?
          # create claimed job, mirroring job request
          current_user.profiles.find_by(profile_type: 0).claimed_jobs.create(job_request_id: @job_request.id)
          was_created = true
          flash[:success] = "Application lodged! Good luck!"
        end
      end

      if was_created == false
        flash[:info] = "Application already submitted. Good luck."
      end
      redirect_to :back

    elsif @job_request.state_machine.current_state == "application_accepted"
      # job is now complete!
      @job_request.state_machine.transition_to!(:completed)
      flash[:success] = "Job completed!"
      redirect_to :back
    end
  end
end
