$(function () {
  window.JobRequests = {
    messageFormInit: function() {
      const $messageForm = $('#message-form')
      const $showMessageForm = $('#show-message-form')

      $messageForm.hide()
      
      $showMessageForm.on('click', function() {
        $messageForm.toggle("slow")
      })
      return false;
    }
  }
});
