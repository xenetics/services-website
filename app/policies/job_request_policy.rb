class  JobRequestPolicy
  attr_reader :current_user, :job_request

  def initialize(current_user, model)
    @current_user = current_user
    @job_request = model
  end

  def index?
    !!current_user
  end

  def show?
    !!current_user
  end

  def create?
    !!current_user
  end

  def new?
    create?
  end

  def edit?
    update?
  end

  def update?
    return false if current_user.nil?
    current_user.profiles.find_by(profile_type: 1).id.eql? job_request.profile_id
  end

  def destroy?
    update?
  end

  class Scope < ApplicationPolicy::Scope
    def resolve
      if current_user_profile.eql? '0'
        scope.all
      else
        scope.all
      end
    end
  end

  private

  def is_in_open_state?
    job_request.state_machine.current_state == 'open'
  end
end
