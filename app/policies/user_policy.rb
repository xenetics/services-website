class UserPolicy
  attr_reader :current_user, :model

  def initialize(current_user, model)
    @current_user = current_user
    @user = model
  end

  # if admin, you can view index
  # else you cannot
  def index?
    @current_user.admin?
  end
end
