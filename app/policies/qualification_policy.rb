class QualificationPolicy
  attr_reader :current_user, :qualification

  def initialize(current_user, model)
    @current_user = current_user
    @qualification = model
  end

  def index?
    !!current_user
  end

  def show?
    index?
  end

  def create?
    !!current_user
  end

  def new?
    create?
  end

  def edit?
    update?
  end

  def update?
    qualification.profile_id == current_user.profiles.first.id unless current_user.nil?
  end

  def destroy?
    update?
  end

  class Scope < ApplicationPolicy::Scope
    def resolve
      if current_user_profile.eql? '0'
        scope.all
      else
        scope.all
      end
    end
  end
end
