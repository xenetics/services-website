class  NotificationPolicy
  attr_reader :current_user, :notification

  def initialize(current_user, model)
    @current_user = current_user
    @notification = model
  end

  def index?
    !!current_user
  end

  def show?
    current_user.id == notification.user_id unless current_user.nil?
  end

  def destroy?
    show?
  end

  class Scope < ApplicationPolicy::Scope
    def resolve
      if current_user_profile.eql? '0'
        scope.all
      else
        scope.all
      end
    end
  end
end
