class JobApplicationPolicy
  attr_reader :current_user, :job_application

  def initialize(current_user, model)
    @current_user = current_user
    @job_application = model
  end

  def index?
    application_for_current_user_job_request?
  end

  def show?
     applicant_is_current_user? || application_for_current_user_job_request?
  end

  def destroy?
    applicant_is_current_user?
  end

  class Scope < ApplicationPolicy::Scope
    def resolve
      if current_user_profile.eql? '0'
        scope.all
      else
        scope.all
      end
    end
  end

  private

  def application_for_current_user_job_request?
    JobRequest.find_by(id: job_application.job_request).profile_id == current_user.profiles.find_by(profile_type: 1).id
  end

  def applicant_is_current_user?
    job_application.user_id == current_user.id
  end
end
