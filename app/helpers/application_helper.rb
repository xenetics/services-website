module ApplicationHelper

  def flash_class(level)
    case level
      when 'success' then "ui positive message"
      when 'error'   then "ui red message"
      when 'info'    then "ui info message"
      when 'warning' then 'ui negative message'
    end
  end

  def format_time(time)
    time.strftime('%x %H:%M')
  end

  def notifications
    current_user.notifications.unseen_notifications.count + job_application_notifications.count
  end

  def job_application_notifications
    user_job_requests = JobRequest.owned_by_current_user(current_user.profiles.find_by(profile_type: 1))
    user_job_request_applications = JobApplication.filter_by_jobs(user_job_requests)
    unseen_applications = user_job_request_applications.filter_by_unseen
  end

  def current_recruiter
    current_user.profiles.find_by(profile_type: 1)
  end

  def current_freelancer
    current_user.profiles.find_by(profile_type: 0)
  end

  def under_recruiter?
    session[:current_profile_id].eql? '1'
  end

  def under_freelancer?
    session[:current_profile_id].eql? '0'
  end

  def full_title(page_title = '')
    base_title = "MyService"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end
end
