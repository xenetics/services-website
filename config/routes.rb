Rails.application.routes.draw do

  resources :qualifications
  resources :skills
  resources :notifications, only: [:show, :index]

  resources :job_requests do
      resource :transitions, as: :transition, only: :update, controller: "job_requests/transitions"
      resources :job_applications, only: [:show, :index, :update]
      resources :comments, only: :create
  end

  resources :profiles, only: [:update, :show] do
    resource :switch_profile, as: :switch, only: :update, controller: 'profiles/switch_profiles'
  end

  devise_for :users, except: :show, :controllers => { registrations: 'registrations' }

  resources :users, only: [] do
    resources :comments, only: :create
  end



  #resource :posts

  get '/users' => 'users#index'
  get '/users/:id' => 'users#show', as: 'user'

  root 'static_pages#home'
  get 'about' => 'static_pages#about'

end
